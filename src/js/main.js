/* ==========================================================================

    Project: Test Project
    Author: 
    Last updated: @@timestamp

   ========================================================================== */

(function($) {

  'use strict';

  var App = {

    /**
     * Init Function
     */
    init: function() {
      App.colorBoxInit();
    },

    /**
     * Colorbox initialization
     */
    colorBoxInit: function() {
      var timeoutId;
      var $links = $('a.gallery');
      $(document).on('cbox_complete', function(a){
       
        clearTimeout(timeoutId);
        if ($links.length === $links.index($.colorbox.element())+1) {
          timeoutId = setTimeout(function() {$.colorbox.close()}, 2000);
        } else {
          timeoutId = setTimeout($.colorbox.next, 2000);
        }
      });
      
      $links.colorbox({
        rel:'gallery',
        open: true
      });

      // setTimeout(function() {
      //   console.log('1')
      // }, 0);
      // console.log('2');
    }
  };

  $(function() {
    App.init();
  });

})(jQuery);